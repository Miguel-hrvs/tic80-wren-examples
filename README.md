Examples for reference creating games in tic80 with wren

Lua to wren in tic80:

1. In wren programs start with: 
 ```
 class Game is TIC  {
  construct new(){}                 
  TIC(){}
 }
```
2. Variables inside a construct should have a _ at the beginning of each one. They work inside their class and behave like global variables. Ex: _a = 1

3. Variables inside methods like TIC(){} behave like local variables and are named different. Ex:
```
var a = 1
```
4. Math functions (methods) in wren work like this: (1).cos instead of cos(1) 

Note: There's no need to call a math module

5. Ranges: 1..5 
6. A for loop with a range of two numbers would be: for (i in 1..5000){}
7. rnd = math.random would be: var rnd = Random.new()
8. Tic80 related functions are called like: TIC.time()
9. The random module is called: 
```
import "random" for Random
```
10. Like in lua, all numbers are treated like doubles, so there's no need to convert number types
11. A random range of integer numbers would require step 9 and: rnd.int(0,240)
12. Check cart 3 to see how to use methods outside the main class in it
13. You can concatenate strings with +
14. to convert a value to string use .toString method
15. Instead of: 
```
 for i,p in pairs(pal) do
  h=30+i/#pal*80-rnd(30)
  mountains(h,p) 
  end
```
  it would be:
```
 for (i in 0...pal.count) {
      var p = pal[i] 
      var h = 30 + i / pal.count * 80 - _rnd.int(30)
      mountains(h, p)
 }
```
In this case _pal was a listin a construct with 3,2,1,0 as values. Then we pass the created variable to iterate through the alist where needed, in this case as a function method.

Another way to do it would be (naming index instead of i):
```
 var index = 0
    for (p in pal) {
      var h = 30 + index / pal.count * 80 - _rnd.int(30)
      mountains(h, p)
      index = index + 1
    }
```
Check cart 46 mountains for more details

16. Instead of # use the .count method Ex: Instead of  h=30+i/#pal*80-rnd(30) use:
```
var h = 30 + i / _pal.count * 80 - _rnd.int(30)
```
17. To use the pi number you can use Num.pi
18. If you find in a lua's list something like this [0]= skip it and put the rest of the values in your wren's list.
19. To store the mouse positions in variables use:
```
var mx=TIC.mouse()[0]
var my=TIC.mouse()[1]
```
20. To represent some characters as strings you have to put \ in front of them. Ex: "\%"
21. Instead of ~ use ^
22. Use .floor to convert to integer instead of //
23. Something like this:
```
m=9
function TIC()
m=math.max(1,m)
```
In wren, would be:
```
class in Game TIC(){
  construct new(){
    _m=9
  }
 TIC(){
  _m=_m.max(1)
 }
}
```
Another possibility would be:
```
var m=9
m.max(1)
```
Or:
```
var m=9
m=m.max(1)
```

24. Never leave TIC.map() empty or it will give errors, Ex:
```
TIC.map(0, 0, 30, 17, 0, 0)
```
25. Instead of # use .count
26. char=t:sub(i,i) would be:
```
char=t[i]
```
27. math.atan2(y1-y0,x1-x0) would be:
```
(y1-y0).atan(x1-x0)
```
28. Instead of: math.randomseed(100):
```
class Game is TIC{
 construct new(){
  _rnd=Random.new
 }
 TIC(){
 _rnd=Random.new(100)
 }
}
```

29. If you encounter a:
```
rnd=math.random
function TIC()
 x0=rnd(10,230)
```
In wren you would do the same as in 28, but instead of the content of the function TIC, you would use:
```
var x0=_rnd.(10,230)
```
30. Instead of ~= use !=
31. To define a range in a for loop or if use .. instead of , Ex:
```
for (c in 0..15){}
```
Instead of:
```
for c=0,15 do
end
```
32. There's a bug with the elli and ellib functions in wren in stable version,
there's a patch ready to be merged in the dev one, if you can't update, use circ instead.
33. A lua table of dictonaries like this:
```
notes=
 {{k=1, l="a", n="C-5"},
 {k=19, l="s", n="D-5"},
 {k=4, l="d", n="E-5"},
 {k=6, l="f", n="F-5"},
 {k=7, l="g", n="G-5"},
 {k=8, l="h", n="A-5"},
 {k=10, l="j", n="B-5"},
 {k=11, l="k", n="C-6"}}
```
In wren would be this list of maps(dictionaries):
```
var notes = [
 {"k": 1, "l": "a", "n": "C-5"},
 {"k": 19, "l": "s", "n": "D-5"},
 {"k": 4, "l": "d", "n": "E-5"},
 {"k": 6, "l": "f", "n": "F-5"},
 {"k": 7, "l": "g", "n": "G-5"},
 {"k": 8, "l": "h", "n": "A-5"},
 {"k": 10, "l": "j", "n": "B-5"},
 {"k": 11, "l": "k", "n": "C-6"}
]
```
To index it you can use something like in step 15:
```
for (i in 0...notes.count) {
 var note = notes[i]
 _c = 12
 var dy = 0
}
```
34. Something like:
```
if keyp(note.k) then
 sfx(0,note.n)
end
```
In wren would be:
```
if (TIC.keyp(note["k"])) {
 TIC.sfx(0, note["n"])
}
```
35. Instead of string.format use .toString Ex instead of:
```
print(string.format("0x%x = %s",addr, val),5,5,12)
```
In wren would be:
```
TIC.print(("0x%(addr) = %(val)").toString, 5, 5, 12)
```
36. Instead of: 
```
res=2^(-k*a)+2^(-k*b)
```
In wren would be:
```
var res=(2).pow(-k*a)+(2).pow(-k*b)
```
37. Something like:
```
 d=dist(px,py)
 if d<0 then
  return 12
 else
  return d
 end
end
```
In wren would be:
```
var d=dist(px,py)
 if (d<0){
  return 12
 }else{
   return d
  }
```